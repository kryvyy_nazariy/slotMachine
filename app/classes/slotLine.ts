import { ISlotPicture, ISlotLine } from "./interfaces/interfaces";


export default class SlotLine {
    id: number;
    length: number;
    posRight: number;
    lineItemArr: ISlotPicture[];    
    slotItemArray: ISlotPicture[];
    ctx: any;
    imageObj: any;
    lineHeight: number;
    canvasGutter: number;
    startPos: number;

    constructor(options: { id: number; length: number; posRight: number; slotItemArray: ISlotPicture[]; ctx: any; imageObj: any; canvasGutter: number; startPos: number
    }) {
        this.id = options.id;
        this.length = options.length;       
        this.posRight = options.posRight;        
        this.lineItemArr = [];     
        this.slotItemArray = options.slotItemArray;
        this.ctx = options.ctx;
        this.imageObj = options.imageObj;
        this.lineHeight = null; //Will fill with value after lineItemArr will be field
        this.canvasGutter = options.canvasGutter;
        this.startPos = options.startPos;
        this.init();
    }

    init() {
        this.createPrivatSlotItemArr(this.slotItemArray, null);
        this.fillSlotLIne();
        this.lineHeightCount();
    }

    getRandom(min: number, max: number) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    drawSlot(slotImage: ISlotPicture): void {
        this.ctx.drawImage(
            this.imageObj,
            slotImage.sourceX,
            slotImage.sourceY,
            slotImage.sourceWidth,
            slotImage.sourceHeight,
            slotImage.destX,
            slotImage.destY,
            slotImage.destWidth,
            slotImage.destHeight
        );
    }

    cloneSlotItem(newObj: any, oldObj: any) {
        for (let key in oldObj) {
            newObj[key] = oldObj[key];
        }
        return newObj;
    }

    fillSlotLIne() {
        let startPos = this.startPos;
        let currentlength = this.lineItemArr.length;
            for (let i = 0; i < currentlength; i++) {
            this.lineItemArr[i].destX = this.posRight;
            this.lineItemArr[i].destY = startPos;
            this.drawSlot(this.lineItemArr[i]);
            startPos = startPos - (this.lineItemArr[i].destHeight + this.canvasGutter);
        }
    }

    createPrivatSlotItemArr(slotItemArray: ISlotPicture[], currentlength: number): void {
        currentlength = currentlength || this.length;
        let itemIndex: number = null;
        for (let i = 0; i < currentlength; i++) {
            itemIndex = this.getRandom(0, slotItemArray.length - 1);
            this.lineItemArr.push(this.cloneSlotItem({}, slotItemArray[itemIndex]));        
        }
    }

    updateItemArray(itemPerLine: number): void {
        this.lineItemArr.splice(0, this.length - itemPerLine);
        // this.lineItemArr.reverse();
        this.createPrivatSlotItemArr(this.slotItemArray,  this.length - itemPerLine);
        this.fillSlotLIne();
    }

    lineHeightCount(): void {
        if(this.lineItemArr[0]){
            let itemHeight = this.lineItemArr[0].destHeight;
            this.lineHeight = this.lineItemArr.length * itemHeight + (this.lineItemArr.length - 1) * 20;
        }
    }
}
