import { ISlotPicture, ISlotLine, ICanvasParams} from './interfaces/interfaces';
import SlotPicture from './slotItem';
import SlotLine from './slotLine';

export default class CanvasFrame {
    slotItemArray: ISlotPicture[];
    slotLineArray: ISlotLine[];
    rollInProgres: boolean;
    canvas: any;
    canvasParams: ICanvasParams;
    imageObj: any;

    constructor(imageObj: any) {
        this.canvas = document.getElementById("myCanvas");
        this.slotItemArray = [];
        this.slotLineArray = [];
        this.rollInProgres = false;
        this.canvasParams = {
            _sourceImageGutter: 20,
            _itemsPerRow: 5,
            _itemsPerLine: 3,
            _canvasGutter: 20
        };        
        this.imageObj = imageObj;// source image obj
        this.init();
    }
    
    init() {
        const ctx = this.canvas.getContext("2d");       
        const startBtn = document.querySelector(".btn");
        
        this.createNewSlotItems();
        this.crteateNewSlotLines(ctx);

        startBtn.addEventListener(
            "click",
            function() {
                this.handleStartBtnClick(ctx);
            }.bind(this)
        );
    } 



    createNewSlotItem(slotItemParam: ISlotPicture): ISlotPicture {
        return new SlotPicture({
        sourceX: slotItemParam.sourceX,
        sourceY: slotItemParam.sourceY,
        sourceWidth: slotItemParam.sourceWidth,
        sourceHeight: slotItemParam.sourceHeight,
        destX: slotItemParam.destX,
        destY: slotItemParam.destY,
        destWidth: slotItemParam.destWidth,
        destHeight: slotItemParam.destHeight,
        id: slotItemParam.id
      });
    }

    createNewSlotItems(): void {
        const _slotsTotalAmount = 9; //Shows how many pictures of slots are on the sourc image  
        const _sourceImageItemsPerRow = 3; // Shows how many pictures of slots are in the row on the sourc image 
        const _sourceImageItemsPerLine = 3; // Shows how many pictures of slots are in the line on the sourc image 
        let sourceX = 0;
        let sourceY = 0;
        let sourceWidth = this.getSlotItemSourceWidth(_sourceImageItemsPerRow);
        let sourceHeight = this.getSlotItemSourceHeight(_sourceImageItemsPerLine);
        let destX = 0;
        let destY = 0;
        let destWidth = this.getSlotItemDestWidth()
        let destHeight = this.getSlotItemDestHeight();
        let id = 0;

        for (let i = 0; i < _slotsTotalAmount; i++) {
            let currentItem = this.createNewSlotItem(
              {sourceX,
              sourceY,
              sourceWidth,
              sourceHeight,
              destX,
              destY,
              destWidth,
              destHeight,
              id}
            );
            this.slotItemArray.push(currentItem);

            sourceX = sourceX + sourceWidth + this.canvasParams._sourceImageGutter;

            if (sourceX > this.imageObj.width) {
                sourceX = 0;
                sourceY = sourceY + sourceHeight + this.canvasParams._sourceImageGutter;
            }            
            id++;
        }
    }

    getSlotItemSourceWidth (itemsPerRow: number): number {  
        return (this.imageObj.width - this.canvasParams._sourceImageGutter * (itemsPerRow - 1)) /
            itemsPerRow;        
    }

    getSlotItemSourceHeight(itemsPerLine: number): number {       
        return (this.imageObj.height - this.canvasParams._sourceImageGutter * (itemsPerLine - 1)) /
            itemsPerLine;        
    }

    getSlotItemDestWidth(): number {
        return (this.canvas.width - this.canvasParams._canvasGutter * (this.canvasParams._itemsPerRow - 1)) / this.canvasParams._itemsPerRow;
    }

    getSlotItemDestHeight(): number {
        return (this.canvas.height - this.canvasParams._canvasGutter * (this.canvasParams._itemsPerLine - 1)) / this.canvasParams._itemsPerLine;
    }

    crteateNewSlotLine(id: number, length: number, posRight: number, ctx: any, imageObj: any,
    canvasGutter: number, startPos: number): ISlotLine {
        return new SlotLine({
          id,
          length,
          posRight,
          slotItemArray: this.slotItemArray,
          ctx,
          imageObj,
          canvasGutter,
          startPos
        });
    }

    crteateNewSlotLines(ctx: any) {
        //build param
        let id = 0; //start slot line id
        let posRight = 0; //start position of the first line
        let lineLength = 10; //length of the first line
        let lineSupplement = 5; // increment value
        let startPos = 440; // start position of images

        for (let i = 0; i < this.canvasParams._itemsPerRow; i++) {
            this.slotLineArray.push(
                this.crteateNewSlotLine(id, lineLength, posRight, ctx, this.imageObj, this.canvasParams._canvasGutter, startPos)
            );
            posRight = posRight + this.slotItemArray[0].destWidth + this.canvasParams._canvasGutter;
            lineLength = lineLength + lineSupplement;
            id++;
        }
    }

    step(progress: number, iterator: number, totalProgres: number, ctx: any): void {
        progress = progress + iterator;       

        this.drawCanvas(iterator, progress , ctx);

        if (progress < totalProgres) {
            requestAnimationFrame(
                function() {
                  this.step(progress, iterator, totalProgres, ctx);
                }.bind(this)
            );
        } else {
            this.updateLines(ctx);
            this.rollInProgres = false;
        }
    }

    rollSlots(speed: number, ctx: any): void {
        let frameHeight = this.canvas.height;
        let minSlotLineHeight = this.slotLineArray[0].lineHeight;
        let iterator = (minSlotLineHeight - frameHeight) / (100 / speed);
        let totalProgres = (this.slotLineArray[this.slotLineArray.length - 1].lineHeight);

        this.step(this.slotLineArray[0].startPos, iterator, totalProgres, ctx,);
    }

    drawCanvas(destY: number,frameHeight: number, ctx: any): void {
        this.clearCtx(ctx);

        this.slotLineArray.forEach((line, index, array) => {
            if (line.lineHeight > frameHeight + (this.slotItemArray[0].destHeight)) {
                line.lineItemArr.forEach(item => {
                    item.destY = item.destY + destY;
                  line.drawSlot(item);
                });
            } else {
                let itemPosition = this.slotLineArray[0].startPos;
                let itemIndexCounter = line.length-1;
                line.lineItemArr.forEach((item, index, array) => {
                    if (index <= line.length - this.canvasParams._itemsPerLine) {
                        item.destY = this.canvas.height+item.destY + destY;
                    }
                    if(index > itemIndexCounter - this.canvasParams._itemsPerLine ){
                        item.destY = itemPosition;
                        itemPosition = itemPosition - (this.canvasParams._sourceImageGutter + item.destHeight);
                        itemIndexCounter++;                        
                    }
                    line.drawSlot(item);
                });
            }
      });
    }

    updateLines(ctx: any) {
        this.clearCtx(ctx);
        this.slotLineArray.forEach(line => {
            line.updateItemArray(this.canvasParams._itemsPerLine);
        });
    }

    clearCtx(ctx: any) {
        ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

    handleStartBtnClick(ctx: any) {
        if (!this.rollInProgres) {
            this.rollInProgres = true;
            this.rollSlots(2, ctx);
        }
    }
}


