import {ISlotPicture} from './interfaces/interfaces';

export default class SlotPicture {
    id: number;
    sourceX: number;
    sourceY: number;
    sourceWidth: number;
    sourceHeight: number;
    destX: number;
    destY: number;
    destWidth: number;
    destHeight: number;

    constructor(options: ISlotPicture) {
      this.id = options.id;
      this.sourceX = options.sourceX;
      this.sourceY = options.sourceY;
      this.sourceWidth = options.sourceWidth;
      this.sourceHeight = options.sourceHeight;
      this.destX = options.destX;
      this.destY = options.destY;
      this.destWidth = options.destWidth;
      this.destHeight = options.destHeight;
    }
}