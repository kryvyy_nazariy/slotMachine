export interface ICanvasParams {
    _sourceImageGutter: number;
    _itemsPerRow: number;
    _itemsPerLine: number;
    _canvasGutter: number
}

export interface ISlotPicture {
    id: number;
    sourceX: number;
    sourceY: number;
    sourceWidth: number;
    sourceHeight: number;
    destX: number;
    destY: number;
    destWidth: number;
    destHeight: number;
}

export interface ISlotLine {
    id: number;
    length: number;
    posRight: number;
    lineHeight: number;
    lineItemArr: any[];
    slotItemArray: any[];
    canvasGutter: number;
    startPos: number;
    drawSlot(slotImage: ISlotPicture): void;
    lineHeightCount(): void;
    updateItemArray(itemPerLine: number): void;
}