import './style.css';
import CanvasFrame from './classes/canvas'

class App {
    constructor() {
        this.init();
    }
    init(){

        const imageObj = new Image();
        imageObj.src = "http://images.all-free-download.com/images/graphiclarge/slot_machine_icons_310830.jpg";
        imageObj.onload = function() {
            new CanvasFrame(imageObj)
        }
    }
}

declare global {
    interface Window { 
        mozRequestAnimationFrame: any;
        msRequestAnimationFrame: any;
    }
}

window.onload = function() {
    (function() {
        let requestAnimationFrame =
            window.requestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.msRequestAnimationFrame;
        window.requestAnimationFrame = requestAnimationFrame;
    })();

    new App();
};
  
